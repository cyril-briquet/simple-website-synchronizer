================================================================
simple-website-synchronizer v0.5 (beta)
Copyright 2009-2024 Cyril Briquet
This program comes with ABSOLUTELY NO WARRANTY.
This is open source software, and you are welcome to redistribute it
under certain conditions; see NOTICE for details.
================================================================

<< Keep the contents and sitemap of your website synchronized
  with the local copy on your hard drive. >>

This software ensures that files of a remote web server are automatically
synchronized with the website's files stored on your hard drive.
Files that are locally modified, renamed, moved around, ... are uploaded,
renamed and moved around on the remote web server.
Files that are locally deleted are also deleted on the remote web server.

The purpose of this software is "to remove the hassle of uploading and deleting
individual files using an FTP (or SFTP) client"
(similarly to the unrelated sitecopy tool).

This software also automatically generates
the website's sitemap (see http://www.sitemaps.org/).

This software requires a Java 21 VM.

================================================================

This software requires a simple configuration file (in properties-based format)
for each website that needs synchronizing.

Usage: provide the path to the configuration file of the website

  ./release/sync-website /path/to/properties-based/configuration/file.props

==> see example.props for the list of supported mandatory properties

(path to the local copy of the website, FTP/SFTP server credentials and URL,
and web server URL for generating the sitemap; if the password is omitted,
the user will be asked to type it each time before synchronizing the website)

==> see example-extended.props for the list of supported optional properties

This configuration file can be stored anywhere and will never be uploaded
to the remote web server.

================================================================

Keywords: website, FTP, SFTP, sitemap, sync, synchronization, Java

Contact: cyril.briquet@canopeer.org

================================================================

