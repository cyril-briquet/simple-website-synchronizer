<?xml version="1.0" encoding="UTF-8"?>

<project xmlns="http://maven.apache.org/POM/4.0.0"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">

<modelVersion>4.0.0</modelVersion>

<groupId>org.bitbucket.simplewebsitesynchronizer</groupId>
<artifactId>simple-website-synchronizer</artifactId>
<packaging>jar</packaging>
<version>0.5beta</version>

<!-- **************************************************************** -->

<name>Simple website synchronizer</name>
<url>https://bitbucket.org/cyril-briquet/simple-website-synchronizer/</url>

<!-- **************************************************************** -->

<developers>
  <developer>
    <id>cyril.briquet</id>
    <name>Cyril Briquet</name>
    <email>cyril.briquet@canopeer.org</email>
    <roles>
      <role>Developer</role>
    </roles>
    <timezone>+1</timezone>
  </developer>
</developers>

<!-- **************************************************************** -->

<properties>
  <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
  <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
  <!--
  <maven.compiler.source>21</maven.compiler.source>
  <maven.compiler.target>21</maven.compiler.target>
  -->
  <maven.build.timestamp.format>yyyyMMddHHmmss</maven.build.timestamp.format>
  <timestamp>${maven.build.timestamp}</timestamp>
</properties>

<!-- **************************************************************** -->

<build>
  <plugins>

    <plugin>
      <groupId>org.apache.maven.plugins</groupId>
      <artifactId>maven-compiler-plugin</artifactId>
      <version>3.11.0</version>
      <configuration>
        <source>21</source>
        <target>21</target>
        <encoding>UTF-8</encoding>
        <meminitial>256m</meminitial>
        <maxmem>2048m</maxmem>
        <compilerArgs>
          <arg>-Xlint:all</arg>
          <arg>--module-path</arg>
          <arg>${project.basedir}/dependencies</arg>
          <arg>-O</arg>
          <arg>-proc:full</arg>
        </compilerArgs>
      </configuration>
    </plugin>

    <plugin>
      <groupId>org.apache.maven.plugins</groupId>
      <artifactId>maven-surefire-plugin</artifactId>
      <version>3.0.0-M7</version>
      <configuration>
        <parallel>methods</parallel>
        <threadCount>16</threadCount>
        <argLine>-Xmx1024M <!-- -Djava.security.manager -Djava.security.policy=src/test/resources/local-security.policy--></argLine>
        <encoding>UTF-8</encoding>
      </configuration>
    </plugin>

    <!-- https://spotbugs.readthedocs.io/en/latest/maven.html -->
    <plugin>
      <groupId>com.github.spotbugs</groupId>
      <artifactId>spotbugs-maven-plugin</artifactId>
      <version>4.8.2.0</version>
      <configuration>
        <excludeFilterFile>${project.basedir}/src/spotbugs-exclude.xml</excludeFilterFile>
      </configuration>
      <executions>
        <execution>
          <phase>package</phase> 
          <goals>
            <goal>check</goal> 
          </goals>
        </execution>
      </executions>
    </plugin>

    <!-- https://maven.apache.org/plugins/maven-clean-plugin/ -->
    <plugin>
    <artifactId>maven-clean-plugin</artifactId>
    <version>3.2.0</version>
    <configuration>
        <filesets>
          <fileset>
            <directory>${project.basedir}/release/dependencies</directory>
            <includes>
                <include>**/*.jar</include>
            </includes>
            <followSymlinks>false</followSymlinks>
          </fileset>
        </filesets>
      </configuration>
    </plugin>

    <!-- https://maven.apache.org/plugins/maven-dependency-plugin/ -->
    <!--
    <plugin>
      <groupId>org.apache.maven.plugins</groupId>
      <artifactId>maven-dependency-plugin</artifactId>
      <version>3.1.1</version>
      <executions>
        <execution>
          <id>copy-dependencies</id>
          <phase>prepare-package</phase>
          <goals>
            <goal>copy-dependencies</goal>
          </goals>
          <configuration>
            <outputDirectory>
              ${project.basedir}/release/dependencies
            </outputDirectory>
          </configuration>
        </execution>
      </executions>
    </plugin>
    -->

    <!-- https://maven.apache.org/plugins/maven-jar-plugin/ -->
    <plugin>
      <groupId>org.apache.maven.plugins</groupId>
      <artifactId>maven-jar-plugin</artifactId>
      <version>3.3.0</version>
      <configuration>
        <archive>
          <manifestFile>src/main/resources/META-INF/MANIFEST.MF</manifestFile>
        </archive>
      </configuration>
    </plugin>

    <!-- https://maven.apache.org/plugins/maven-assembly-plugin/usage.html -->
    <plugin>
      <artifactId>maven-assembly-plugin</artifactId>
      <version>3.1.0</version>
      <configuration>
        <descriptorRefs>
          <descriptorRef>jar-with-dependencies</descriptorRef>
        </descriptorRefs>
        <archive>
          <manifest>
            <mainClass>org.bitbucket.simplewebsitesynchronizer.SimpleWebsiteSynchronizer</mainClass>
          </manifest>
        </archive>
      </configuration>
      <executions>
        <execution>
          <id>make-assembly</id> <!-- this is used for inheritance merges -->
          <phase>package</phase> <!-- bind to the packaging phase -->
          <goals>
            <goal>single</goal>
          </goals>
        </execution>
      </executions>
    </plugin>

  </plugins>

  <resources>
    <resource>
      <directory>src/main/resources</directory>
      <targetPath>resources</targetPath>
      <filtering>true</filtering> <!-- so that the POM's timestamp property can be resolved in the timestamp resource file -->
    </resource>
  </resources>

  <testResources>
    <testResource>
      <directory>src/test/resources</directory>
      <targetPath>test-resources</targetPath>
    </testResource>
  </testResources>

</build>

<!-- **************************************************************** -->

<!--
<reporting>
  <plugins>

    <plugin>
      <groupId>org.codehaus.mojo</groupId>
      <artifactId>cobertura-maven-plugin</artifactId>
      <version>2.4</version>
    </plugin>

  </plugins>
</reporting>
-->

<!-- **************************************************************** -->

<dependencies>

  <!-- https://mvnrepository.com/artifact/com.github.mwiede/jsch -->
  <dependency>
    <groupId>com.github.mwiede</groupId>
    <artifactId>jsch</artifactId>
    <version>0.2.9</version>
  </dependency>

  <!-- https://mvnrepository.com/artifact/commons-net/commons-net -->
  <dependency>
    <groupId>commons-net</groupId>
    <artifactId>commons-net</artifactId>
    <version>3.8.0</version>
  </dependency>

  <!-- https://mvnrepository.com/artifact/com.github.spotbugs/spotbugs -->
  <dependency>
    <groupId>com.github.spotbugs</groupId>
    <artifactId>spotbugs</artifactId>
    <version>4.8.3</version>
  </dependency>

  <!-- https://mvnrepository.com/artifact/org.testng/testng -->
  <dependency>
    <groupId>org.testng</groupId>
    <artifactId>testng</artifactId>
    <version>7.8.0</version>
  </dependency>

</dependencies>

<!-- **************************************************************** -->

</project>

