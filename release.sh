#/bin/bash

mvn clean package
BUILD_SUCCESS=$?

if [ ${BUILD_SUCCESS} == 0 ] ; then
  echo
  echo "Releasing simple-website-synchronizer.jar"
  cp target/simple-website-synchronizer-*-jar-with-dependencies.jar release/simple-website-synchronizer.jar
fi


