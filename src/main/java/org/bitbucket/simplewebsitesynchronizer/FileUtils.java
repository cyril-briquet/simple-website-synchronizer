/*
 /*
 * Copyright 2009-2018 Cyril Briquet.
 * 
 * Licensed under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *  
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package org.bitbucket.simplewebsitesynchronizer;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import static java.nio.file.StandardOpenOption.CREATE;
import static java.nio.file.StandardOpenOption.TRUNCATE_EXISTING;
import static java.nio.file.StandardOpenOption.WRITE;
import java.util.Properties;

/**
 * @author Cyril Briquet
 */
public class FileUtils {

	public static final String CANONICAL_FILE_SEP = "/";
    
	// ****************************************************************

	/** Writes data to the specified file.
     * @param fn filename
     * @param data data
     */
    public static void writeFile(String fn, String data) throws IOException {

    	if (fn == null) {
    		throw new NullPointerException("illegal filename");
    	}
    	if (data == null) {
    		throw new NullPointerException("illegal data");
    	}
    	
        PrintWriter pw = null;
        try {
            pw = new PrintWriter(Files.newBufferedWriter(FileSystems.getDefault().getPath(fn), CREATE, WRITE, TRUNCATE_EXISTING));
            pw.print(data);
			pw.flush();
		}
        finally {
        	if (pw != null) {
        		try {
        			pw.close();
        		}
        		catch (Exception e) {}
        	}
        }		
		
	}
    
	// ****************************************************************

    /** Canonicalizes file separators with UNIX-style slashes
     * @param s String
     * @return canonicalized String
     */
    public static String canonicalizeFileSeparators(String s) {

    	if (s == null) {
    		throw new NullPointerException("null String");
    	}
    	
    	return s.replace('\\', '/');

    }

	// ****************************************************************

	public static Properties loadProperties(String fn) throws IOException {
		
    	if (fn == null) {
    		throw new NullPointerException("illegal filename");
    	}

		Properties props = null;
		
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(fn);
			props = new Properties();
			props.load(fis);
		}
		finally {
			if (fis != null) {
				fis.close();
			}
		}
		
		return props;
		
	}

}
