/*
 /*
 * Copyright 2009-2018 Cyril Briquet.
 * 
 * Licensed under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *  
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package org.bitbucket.simplewebsitesynchronizer.sitemap;

import java.util.Map;

/** Sitemap factory.
 * @author Cyril Briquet
 */
public class SitemapFactory {

	// ****************************************************************

	/** Creates a sitemap.
     * @param websiteBaseDirectory website base directory
     * @param webServerURL web server URL
     * @param unmappedFiles unmapped files (keys are filenames, values are flags indicating whether the key designates a directory)
     * @param sitemapPhpUrlRewriting if true, enable URL rewriting for PHP files
	 * @return a new sitemap
	 */
	public Sitemap createSitemap(String websiteBaseDirectory, String websiteURL, Map<String, Boolean> unmappedFiles,
								 boolean sitemapPhpUrlRewriting) {

		if (websiteBaseDirectory == null) {
			throw new NullPointerException("illegal website base directory");
		}
		if (websiteURL == null) {
			throw new NullPointerException("illegal website URL");
		}
		if (unmappedFiles == null) {
			throw new NullPointerException("illegal unmapped files");
		}

		return new Sitemap(websiteBaseDirectory, websiteURL, unmappedFiles, sitemapPhpUrlRewriting);
		
	}
	
	// ****************************************************************
	
}
