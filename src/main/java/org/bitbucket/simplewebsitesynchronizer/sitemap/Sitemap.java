/*
 * Copyright 2009-2018 Cyril Briquet.
 * 
 * Licensed under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *  
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package org.bitbucket.simplewebsitesynchronizer.sitemap;

import static org.bitbucket.simplewebsitesynchronizer.FileUtils.writeFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/** Sitemap.
 * @author Cyril Briquet
 */
public class Sitemap {

	public static final int MAX_SIZE = 50000; // the Sitemap protocol supports at most 50000 URLs
	
	private final static double hiPriority = 1.0;
	private final static double regularPriority = 0.5;

	// ****************************************************************

	private final String websiteBaseDirectory;
	private final String webServerURL;
    private final Map<String, Boolean> unmappedFiles;
    private final boolean sitemapPhpUrlRewriting;
    
    private final List<SitemapItem> items;
    
	// ****************************************************************
	
    /** Constructs a new sitemap.
     * @param websiteBaseDirectory website base directory
     * @param webServerURL web server URL
     * @param unmappedFiles unmapped files (keys are filenames, values are flags indicating whether the key designates a directory)
     * @param sitemapPhpUrlRewriting if true, enable URL rewriting for PHP files
     */
	protected Sitemap(String websiteBaseDirectory, String webServerURL, Map<String, Boolean> unmappedFiles, boolean sitemapPhpUrlRewriting) {
		
		if (websiteBaseDirectory == null) {
			throw new NullPointerException("illegal website base directory");
		}
		if (webServerURL == null) {
			throw new NullPointerException("illegal web server URL");
		}
		if (webServerURL.endsWith("/") == false) {
			throw new IllegalArgumentException("illegal web server URL");
		}
		if (unmappedFiles == null) {
			throw new NullPointerException("illegal unmapped files");
		}
		
		this.websiteBaseDirectory = websiteBaseDirectory;
		this.webServerURL = webServerURL;
		this.unmappedFiles = unmappedFiles;
		this.sitemapPhpUrlRewriting = sitemapPhpUrlRewriting;
		
		this.items = new ArrayList<SitemapItem>();
		
	}
	
	// ****************************************************************

	@Override
	public synchronized String toString() {
		
		if (this.items.size() > MAX_SIZE) {
			throw new IllegalStateException("sitemap proptocol limitation");
		}
		
		final StringBuilder sb = new StringBuilder();
		
		sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>").append("\n");
		sb.append("<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\" >").append("\n");
		
		for (SitemapItem item : this.items) {
			sb.append(item);
		}
		
		sb.append("</urlset>").append("\n\n");
		
		return sb.toString();
		
	}
	
	// ****************************************************************

	
	/** Adds the web server root URL to the sitemap.
	 * @param timestamp timestamp
	 */
	public synchronized void addRootURL(SitemapTimestamp timestamp) {
		
		if (timestamp == null) {
			throw new NullPointerException("illegal timestamp");
		}
		
		this.items.add(new SitemapItem(this.webServerURL, timestamp, "1.0")); 
		
	}
	
	/** Adds a URL to the sitemap.
	 * @param url URL
	 * @param timestamp timestamp
	 */
	public synchronized void addURL(String url, SitemapTimestamp timestamp) {
		
		if (this.items.size() > MAX_SIZE) {
			throw new IllegalStateException("sitemap proptocol limitation");
		}
		if (url == null) {
			throw new NullPointerException("illegal URL");
		}
		if (url.startsWith(this.webServerURL)) {
			throw new IllegalArgumentException("illegal URL " + url + ", should be absolute, not starting with " + this.webServerURL);
		}
		if (timestamp == null) {
			throw new NullPointerException("illegal timestamp");
		}
		
		if (this.unmappedFiles.get(url) != null) {
			System.out.println("\tUnmapped file "+url);
			return;
		}
    	for (Map.Entry<String, Boolean> entry : this.unmappedFiles.entrySet()) {
    		final String unmappedFile = entry.getKey();
    		if (url.startsWith(unmappedFile) && (this.unmappedFiles.get(unmappedFile) == true)) {
    			return;
    		}
    	}
		
		final String priority = Double.toString(isHighPriorityURL(url) ? hiPriority : regularPriority);
		
		url = this.webServerURL + url;
		
		if ((this.sitemapPhpUrlRewriting) && // enable URL rewriting for PHP files
			url.endsWith(".php")) {
			url = url.substring(0, url.length() - 4); // removes the .php extension from the sitemap entry
		}
		
		this.items.add(new SitemapItem(url, timestamp, priority));
		
	}
	
	// ****************************************************************
	
	/** Persists the sitemap.
	 * @throws IOException if an exception occurs while persisting the sitemap
	 */
	public synchronized void persist() throws IOException {
		
		// write to websiteBaseDirectory/sitemap.xml
		writeFile(this.websiteBaseDirectory + "/" + "sitemap.xml", toString());
		
	}
	
	// ****************************************************************
	
	/** Notifies search engines that the sitemap has been updated.
	 */
	@Deprecated
	public synchronized void notifySearchEngines() {
	
		new SitemapNotifier(this.webServerURL).notifySearchEngines();
		
	}
	
	// ****************************************************************
	
	/** Returns true if search engines should be notified to index the URL with high priority.
	 * @param url URL
	 * @return true if search engines should be notified to index the URL with high priority
	 */
	private static boolean isHighPriorityURL(String url) {
		
		if (url == null) {
			throw new NullPointerException("illegal URL");
		}
		if (url.length() < 1) {
			throw new IllegalArgumentException("illegal URL");
		}

		if (url.equals("/")) {
			return true;
		}
		if (url.indexOf("/") > -1) {
			return false;
		}
		
		return
			url.endsWith("index.html") || url.endsWith("index") ||
			url.endsWith("index.php") || url.endsWith("php") ||
			url.endsWith("welcome.html") || url.endsWith("welcome");
		
	}
	
	// ****************************************************************
	
}
