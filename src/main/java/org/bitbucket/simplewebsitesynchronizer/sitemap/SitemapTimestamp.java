/*
 * Copyright 2009-2018 Cyril Briquet.
 * 
 * Licensed under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *  
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package org.bitbucket.simplewebsitesynchronizer.sitemap;

import java.text.SimpleDateFormat;
import java.util.Date;

/** Sitemap timestamp.
 * @author Cyril Briquet
 */
public class SitemapTimestamp {
	
	private final String timestamp; // string representation in the sitemap protocol of a timestamp
	
	// ****************************************************************
	
	/** Constructs a new sitemap timestamp.
	 */
	public SitemapTimestamp() {
		
		this.timestamp = 	//new SimpleDateFormat("yyyy-MM-dd"). // also allowed by protocol
			new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.000XXX"). // example: YYYY-MM-DDTHH:MM:SS.mmm+01:00
			format(new Date());
		
	}
	
	/** Constructs a new sitemap timestamp.
	 * @param timestamp timestamp
	 */
	public SitemapTimestamp(String timestamp) {
		
		if (timestamp == null) {
			throw new NullPointerException("illegal timestamp");
		}
		if (timestamp.length() < 1) {
			throw new IllegalArgumentException("illegal timestamp");
		}
		
		this.timestamp = timestamp;

	}
	
	// ****************************************************************
	
	@Override
	public String toString() {
		
		return this.timestamp;
		
	}
	
	// ****************************************************************
	
}
