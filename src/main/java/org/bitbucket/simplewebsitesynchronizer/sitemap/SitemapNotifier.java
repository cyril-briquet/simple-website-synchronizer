/*
 * Copyright 2009-2018 Cyril Briquet.
 * 
 * Licensed under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *  
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

// SitemapNotifier.java

package org.bitbucket.simplewebsitesynchronizer.sitemap;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

/** Sitemap notifier.
 * @author Cyril Briquet
 */
@Deprecated
class SitemapNotifier {
	
	// ****************************************************************
	
	private static final String[] searchEngineURLs = new String[] { // source: http://en.wikipedia.org/wiki/Sitemaps
		"http://www.google.com/webmasters/tools/",
		//"http://www.bing.com/"
		//legacy code -- "http://search.yahooapis.com/SiteExplorerService/V1/updateNotification?appid=SitemapWriter&url="
	};
	
	// ****************************************************************
	
	private final String webServerURL;
	
	// ****************************************************************
	
	/** Constructs a new sitemap notifier.
	 * @param webServerURL web server URL
	 */
	public SitemapNotifier(String webServerURL) {
		
		if (webServerURL == null) {
			throw new NullPointerException("illegal web server URL");
		}
		if (webServerURL.endsWith("/") == false) {
			throw new IllegalArgumentException("illegal web server URL");
		}
		if (webServerURL.length() < 1) {
			throw new IllegalArgumentException("illegal web server URL");
		}
		
		this.webServerURL = webServerURL;
		
	}
	
	// ****************************************************************
	
	/** Notifies search engines that the underlying sitemap has been updated.
	 */
	public synchronized void notifySearchEngines() {
		
		for (String searchEngineURL : searchEngineURLs) {
			final String sitemapUrl = searchEngineURL +
				(searchEngineURL.endsWith("/") ? "" : "/") + "ping?sitemap=" + this.webServerURL + "sitemap.xml";
			notifySearchEngine(sitemapUrl);
		}
		
	}

	/** Surfs on (i.e. retrieve) the search engine web page identified by the specified URL.
	 * @param searchEngineURL search engine URL
	 */
	private static void notifySearchEngine(String searchEngineURL) {
		
		if (searchEngineURL == null) {
			throw new NullPointerException("illegal search engine URL");
		}
		
		try {
			final URL url = new URI(searchEngineURL).toURL();
			final HttpURLConnection httpCon = (HttpURLConnection) url.openConnection();
			httpCon.setDoOutput(true);
			httpCon.connect();
			System.out.println("\t" + ((httpCon.getResponseCode() == 200) ? "Submitted" : "Could not submit") + " sitemap to " + searchEngineURL);
			httpCon.disconnect();
		}
		catch (MalformedURLException | URISyntaxException e) {
			throw new IllegalStateException("illegal search engine URL " + searchEngineURL);
		}
		catch (IOException e) {
			System.out.println("\tCould not submit sitemap to " + searchEngineURL + "\n\t" + e);
		}
		
	}
	
	// ****************************************************************
	
}
