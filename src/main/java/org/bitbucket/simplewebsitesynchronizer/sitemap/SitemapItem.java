/*
 * Copyright 2009-2018 Cyril Briquet.
 * 
 * Licensed under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *  
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package org.bitbucket.simplewebsitesynchronizer.sitemap;

/** Sitemap item.
 * @author Cyril Briquet
 */
class SitemapItem {
	
	private final String string; // string (XML) representation in the sitemap protocol of the stored sitemap item
	
	// ****************************************************************
	
	/** Constructs a new sitemap item.
	 * @param url URL
	 * @param timestamp timestamp
	 * @param priority indexing priority
	 */
	public SitemapItem(String url, SitemapTimestamp timestamp, String priority) {
		
		if (url == null) {
			throw new NullPointerException("illegal URL");
		}
		if (timestamp == null) {
			throw new NullPointerException("illegal timestamp");
		}
		if (priority == null) {
			throw new NullPointerException("illegal priority");
		}
		
		final StringBuilder sb = new StringBuilder();
		
		sb.append("<url>").append("\n");
		sb.append("  <loc>").append(url).append("</loc>").append("\n");
		sb.append("  <lastmod>").append(timestamp).append("</lastmod>").append("\n");
		sb.append("  <priority>").append(priority).append("</priority>").append("\n");
		sb.append("</url>").append("\n");
		
		this.string = sb.toString();
		
	}
	
	// ****************************************************************
	
	@Override
	public String toString() {
		
		return this.string;
		
	}
	
	// ****************************************************************
	
}
