/*
 * Copyright 2009-2018 Cyril Briquet.
 * 
 * Licensed under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *  
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package org.bitbucket.simplewebsitesynchronizer;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

import static org.bitbucket.simplewebsitesynchronizer.SimpleWebsiteSynchronizerFactory.DEFAULT_PERSISTED_STATE_PATH;
import org.bitbucket.simplewebsitesynchronizer.sitemap.Sitemap;
import org.bitbucket.simplewebsitesynchronizer.state.FileMetadata;
import org.bitbucket.simplewebsitesynchronizer.state.FileSignatureFactory;
import org.bitbucket.simplewebsitesynchronizer.state.WebsiteState;
import org.bitbucket.simplewebsitesynchronizer.state.WebsiteStateFactory;
import org.bitbucket.simplewebsitesynchronizer.upload.WebsiteUploader;

import java.io.File;
import java.io.IOException;
import java.util.Map;

/**
 * @author Cyril Briquet
 */
public class SimpleWebsiteSynchronizer {

	// ****************************************************************

	private final String websiteBaseDirectory;
    private final String persistedStatePath;
    private final Sitemap sitemap;
    private final boolean doUpload;
    private final WebsiteUploader websiteUploader;
    private final Map<String,Boolean> ignoredFiles;

	// ****************************************************************

	protected SimpleWebsiteSynchronizer(String websiteBaseDirectory, String persistedStatePath, Sitemap sitemap,
										Map<String, Boolean> ignoredFiles, boolean doUpload) {
		
		this(websiteBaseDirectory, persistedStatePath, sitemap, ignoredFiles, doUpload, null);
		
	}

	protected SimpleWebsiteSynchronizer(String websiteBaseDirectory, String persistedStatePath, Sitemap sitemap,
										Map<String, Boolean> ignoredFiles, boolean doUpload, WebsiteUploader websiteUploader) {
		
		if (websiteBaseDirectory == null) {
			throw new NullPointerException("illegal website base directory");
		}
		if ((websiteUploader == null) && (doUpload == true)) {
			throw new NullPointerException("illegal website uploader");
		}
		if ((websiteUploader != null) && (doUpload == false)) {
			throw new IllegalArgumentException("illegal website uploader");
		}
		if (ignoredFiles == null) {
			throw new NullPointerException("illegal ignored files");
		}
		
		this.websiteBaseDirectory = websiteBaseDirectory;
		this.persistedStatePath = persistedStatePath;
		this.sitemap = sitemap;
		this.doUpload = doUpload; 
		this.websiteUploader = websiteUploader;
		this.ignoredFiles = ignoredFiles;
		
	}
	
	// ****************************************************************

    /** Synchronizes website local files with website remote files.
     * @throws IOException if an error occurs while uploading the website
     */
    public synchronized void sync() throws IOException {

		final WebsiteState websiteState = loadWebsiteState();

		if (this.doUpload) {
			
			generateSitemap(websiteState, this.sitemap);
			websiteState.persist();
		    System.out.println("\n\tSitemap synchronized.\n");
			
			this.websiteUploader.upload(websiteState);
			websiteState.persist();
		    System.out.println("\n\tWebsite synchronized.\n");
		    
			// deprecated -- this.sitemap.notifySearchEngines();
			
		}
		else { // eval only

			generateSitemap(websiteState, this.sitemap);
			websiteState.persist();
		    System.out.println("\n\tSitemap synchronized.\n");
			
			System.out.println("\n\tWebsite synchronized.\n");
			
		}
		
    }

    // ****************************************************************

    private synchronized WebsiteState loadWebsiteState() throws IOException {

    	// STEP 1: ensure that sitemap.xml exists so that it is included in the website state
    	
    	/*legacy code
    	final File sitemapDir = new File(this.websiteBaseDirectory, "sitemap");
    	if (sitemapDir.exists() == false) {
    		if (sitemapDir.mkdir() == false) {
    			throw new IOException("could not create sitemap directory");
    		}
    	}
    	FileUtils.writeFile(sitemapDir + FileUtils.FILE_SEP + "sitemap.xml", String.valueOf(System.currentTimeMillis()));
    	 */
    	
    	FileUtils.writeFile(this.websiteBaseDirectory + FileUtils.CANONICAL_FILE_SEP + "sitemap.xml", String.valueOf(System.currentTimeMillis()));
    	
    	// STEP 2: create the website state
    	
		final FileSignatureFactory fileSignatureFactory = new FileSignatureFactory(this.websiteBaseDirectory);
		final WebsiteStateFactory websiteStateFactory = new WebsiteStateFactory(fileSignatureFactory);
		final WebsiteState websiteState = websiteStateFactory.createWebsiteState(this.websiteBaseDirectory, this.persistedStatePath, this.ignoredFiles);

		return websiteState;
		
    }    
    
    private static void generateSitemap(WebsiteState websiteState, Sitemap sitemap) throws IOException {
    	
    	if (websiteState == null) {
    		throw new NullPointerException("illegal website state");
    	}
    	if (sitemap == null) {
    		throw new NullPointerException("illegal sitemap");
    	}
    	
    	sitemap.addRootURL(websiteState.getTimestamp()); // add server root
    	
		for (FileMetadata fileMetadata : websiteState) {
			final String url = fileMetadata.getFileSignature().getFilePath();
			
			switch (fileMetadata.getFileState()) {
			case to_upload:
				sitemap.addURL(url, websiteState.getTimestamp()); // use the "now" timestamp
				break;
			case synced:
				sitemap.addURL(url, fileMetadata.getFileTimestamp()); // retrieve the existing timestamp
				break;
			case to_delete:
				//skip;
				break;
			default:
				throw new IllegalStateException("illegal state");
			}
		}
		
		sitemap.persist(); // before persisting website state (so that the updated sitemap.xml is taken into account)
		
    }
    
    // ****************************************************************

    public static void main(String[] args) throws IOException {

		System.out.println("== simple-website-synchronizer v0.5 (beta) =====================");
		System.out.println("Copyright 2009-2024 Cyril Briquet");
		System.out.println("This program comes with ABSOLUTELY NO WARRANTY.");
		System.out.println("This is open source software, and you are welcome to redistribute it"); 
		System.out.println("under certain conditions; see NOTICE for details.");
		System.out.println("================================================================");

    	if (args.length != 2) {
		    usage();
		}
    	
    	final String configPath = args[0];
    	final String action = args[1];
    	boolean doUpload = true;
    	
    	if (action.equals("reset")) {
    		if (new File(configPath).delete()) {
    			System.out.println("\n\tWebsite state reset.\n");
    		}
    		else {
    			System.out.println("\n\tCould not reset website state (" + DEFAULT_PERSISTED_STATE_PATH + "), verify permissions and retry.\n");
    		}
    		return;
    	}
    	else if (action.equals("upload")) {
    		doUpload = true;
    	}
    	else if (action.equals("eval")) {
    		doUpload = false;
    	}
    	else {
    		usage();
    	}
    	
    	final SimpleWebsiteSynchronizerFactory websiteSyncFactory = new SimpleWebsiteSynchronizerFactory();
		final SimpleWebsiteSynchronizer websiteSync = websiteSyncFactory.createWebsiteSynchronizer(configPath, doUpload);
		websiteSync.sync();
	
    }
    
    @SuppressFBWarnings("DM_EXIT")
    private static void usage() {
    	
	    System.out.println("Usage : ./sync-website <path to config file> <action flag: upload|reset>\n");
	    System.exit(0);
	    
    }

    // ****************************************************************

}
