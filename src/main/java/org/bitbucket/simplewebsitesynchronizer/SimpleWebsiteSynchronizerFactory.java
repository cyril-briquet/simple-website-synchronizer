/*
 * Copyright 2009-2018 Cyril Briquet.
 * 
 * Licensed under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *  
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package org.bitbucket.simplewebsitesynchronizer;

import static org.bitbucket.simplewebsitesynchronizer.FileUtils.loadProperties;
import org.bitbucket.simplewebsitesynchronizer.sitemap.Sitemap;
import org.bitbucket.simplewebsitesynchronizer.sitemap.SitemapFactory;
import org.bitbucket.simplewebsitesynchronizer.upload.WebsiteUploader;
import org.bitbucket.simplewebsitesynchronizer.upload.WebsiteUploaderFactory;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;

/**
 * @author Cyril Briquet
 */
public class SimpleWebsiteSynchronizerFactory {

    public static final String DEFAULT_PERSISTED_STATE_PATH = ".swsrc";

	// ****************************************************************

	public SimpleWebsiteSynchronizer createWebsiteSynchronizer(String configPath, boolean doUpload) throws IOException {
	
		if (configPath == null) {
			throw new NullPointerException("null config path");
		}
		
		// STEP 1: determine working paths
		
		final Properties props = loadProperties(configPath);

		String websiteBaseDirectory = props.getProperty("website-base-directory");
		if (websiteBaseDirectory == null) {
			throw new NullPointerException("illegal website base directory");
		}
		if (websiteBaseDirectory.endsWith(FileUtils.CANONICAL_FILE_SEP) == false) {
		    websiteBaseDirectory += FileUtils.CANONICAL_FILE_SEP;
		}
		if (new File(websiteBaseDirectory).exists() == false) {
			throw new IOException("specifed website base directory " + websiteBaseDirectory + " does not exist");
		}

		String webServerURL = props.getProperty("web-server-url");
		if (webServerURL == null) {
			throw new NullPointerException("illegal web server URL");
		}
		if (webServerURL.startsWith("http://") == false) {
			webServerURL = "http://" + webServerURL ;
		}
		if (webServerURL.endsWith("/") == false) {
			webServerURL += "/";
		}

		String persistedStatePath = props.getProperty("persisted-state-path");
		if (persistedStatePath == null) {
			persistedStatePath = DEFAULT_PERSISTED_STATE_PATH;
		}
		else {
			//throw new UnsupportedOperationException("yet to be implemented");
		}
		
		// STEP 2: load unmapped and ignored files map
		
		// load unmapped files from configuration properties
		final String unmappedFilesProperty = props.getProperty("unmapped-files");
		final String[] unmappedFilesList = ((unmappedFilesProperty != null) ? unmappedFilesProperty : "").split(",");
		final Map<String, Boolean> unmappedFiles = getUnmappedFiles(websiteBaseDirectory, unmappedFilesList);

		// load ignored files from configuration properties
		final String ignoredFilesProperty = props.getProperty("ignored-files");
		final String[] ignoredFilesList = ((ignoredFilesProperty != null) ? ignoredFilesProperty : "").split(",");
		final Map<String, Boolean> ignoredFiles = getIgnoredFiles(websiteBaseDirectory, ignoredFilesList, persistedStatePath, configPath, unmappedFiles);
		
		// STEP 3: create (empty) sitemap data structure
		
	    if (props.getProperty("sitemap-php-url-rewriting") == null) {
	    	throw new NullPointerException("illegal sitemap PHP url rewriting flag");
		}
		if (/*(props.getProperty("sitemap-php-url-rewriting") != null) &&*/
	    	(props.getProperty("sitemap-php-url-rewriting").equalsIgnoreCase("on") == false) &&
	    	(props.getProperty("sitemap-php-url-rewriting").equalsIgnoreCase("off") == false)) {
	    	throw new IllegalArgumentException("illegal sitemap PHP url rewriting flag " + props.getProperty("sitemap-php-url-rewriting"));
	    }
	    final boolean sitemapPhpUrlRewriting = props.getProperty("sitemap-php-url-rewriting").equalsIgnoreCase("on");
	    
		final SitemapFactory sitemapFactory = new SitemapFactory();
		final Sitemap sitemap = sitemapFactory.createSitemap(websiteBaseDirectory, webServerURL, unmappedFiles, sitemapPhpUrlRewriting);

		// STEP 4: create website synchronizer
		
		if (doUpload) {
			final WebsiteUploaderFactory websiteUploaderFactory = new WebsiteUploaderFactory();
			final WebsiteUploader websiteUploader = websiteUploaderFactory.createFTPWebsiteUploader(props);
			return new SimpleWebsiteSynchronizer(websiteBaseDirectory, persistedStatePath, sitemap, ignoredFiles, doUpload, websiteUploader);
		}
		else { // eval only
			return new SimpleWebsiteSynchronizer(websiteBaseDirectory, persistedStatePath, sitemap, ignoredFiles, doUpload);
		}

	}
	
	// ****************************************************************

	private static Map<String, Boolean> getUnmappedFiles(String websiteBaseDirectory, String[] unmappedFilesList) {
		
		if (websiteBaseDirectory == null) {
			throw new NullPointerException("illegal website base directory");
		}
		if (unmappedFilesList == null) {
			throw new NullPointerException("illegal unmapped files list");
		}
		
		final Map<String, Boolean> unmappedFiles = new TreeMap<String,Boolean>();
		
		for (String unmappedFile : unmappedFilesList) {
			final String lemmatizedUnmappedFile = unmappedFile.trim();
			if (lemmatizedUnmappedFile.length() == 0) {
				continue;
			}
			
			final String contextualizedUnmappedFile = websiteBaseDirectory + lemmatizedUnmappedFile;
			final boolean isDirectory = new File(contextualizedUnmappedFile).isDirectory();
			if (unmappedFiles.put(lemmatizedUnmappedFile, isDirectory) != null) {
				System.out.println("\twarning: file " + lemmatizedUnmappedFile + " requested multiple times to be unmapped");
			}
		}

		return unmappedFiles;
		
	}
	
	private static Map<String, Boolean> getIgnoredFiles(String websiteBaseDirectory, String[] ignoredFilesList,
														String persistedStatePath, String configPath, Map<String, Boolean> unmappedFiles) {

		if (websiteBaseDirectory == null) {
			throw new NullPointerException("illegal website base directory");
		}
		if (ignoredFilesList == null) {
			throw new NullPointerException("illegal ignored files list");
		}
		if (persistedStatePath == null) {
			throw new NullPointerException("illegal persisted state path");
		}
		persistedStatePath = persistedStatePath.trim();
		if (persistedStatePath.length() < 1) {
			throw new IllegalArgumentException("illegal persisted state path");
		}
		if (configPath == null) {
			throw new NullPointerException("illegal config path");
		}
		configPath = configPath.trim();
		if (configPath.length() < 1) {
			throw new IllegalArgumentException("illegal config path");
		}
		if (unmappedFiles == null) {
			throw new NullPointerException("illegal unmapped files");
		}

		final Map<String, Boolean> ignoredFiles = new TreeMap<String, Boolean>();
		
		// ignore .swsrc
		
		ignoredFiles.put(websiteBaseDirectory + persistedStatePath, false);
		unmappedFiles.put(websiteBaseDirectory + persistedStatePath, false);
		
		// ignore properties file (if stored in website directory)
		
		try {
			final String lemmatizedConfigPath = new File(configPath).getCanonicalPath();
			if (lemmatizedConfigPath.startsWith(websiteBaseDirectory)) {
				ignoredFiles.put(lemmatizedConfigPath, false);
				unmappedFiles.put(lemmatizedConfigPath, false);
			}
		}
		catch (IOException e) {
			throw new IllegalStateException("unexpected error");
		}
		
		// ignore specified files
		
		for (String ignoredFile : ignoredFilesList) {
			final String lemmatizedIgnoredFile = ignoredFile.trim();
			if (lemmatizedIgnoredFile.length() == 0) {
				continue;
			}
			
			final String contextualizedIgnoredFile = websiteBaseDirectory + lemmatizedIgnoredFile;
			final boolean isDirectory = new File(contextualizedIgnoredFile).isDirectory();
			if (ignoredFiles.put(contextualizedIgnoredFile, isDirectory) != null) {
				System.out.println("\twarning: file " + lemmatizedIgnoredFile + " requested multiple times to be ignored");
			}
			unmappedFiles.put(lemmatizedIgnoredFile, isDirectory);
		}
		
		// unmappedFiles updated by side effect as well
		
		return ignoredFiles;

	}

}
