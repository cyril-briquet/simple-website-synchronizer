/*
 * Copyright 2009-2018 Cyril Briquet.
 * 
 * Licensed under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *  
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package org.bitbucket.simplewebsitesynchronizer.upload;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;

/** FTP implementation of the FTP agent interface.
 * @author Cyril Briquet
 */
class PlainFTPClient implements FTPClientAdapter {
	
    private final FTPCredentials ftpCredentials;
    private final FTPClient ftpClient;
    
    // ****************************************************************
	
	public PlainFTPClient(FTPCredentials ftpCredentials) {
		
		if (ftpCredentials == null) {
			throw new NullPointerException("illegal FTP credentials");
		}
		
		this.ftpCredentials = ftpCredentials;
		this.ftpClient = new FTPClient();
		
	}
	
	// ****************************************************************

    public synchronized boolean chmod(String remotePath, int permissions) throws IOException {
    	
    	if (remotePath == null) {
    		throw new NullPointerException("illegal remote path");
    	}
    	if (remotePath.length() < 1) {
    		throw new IllegalArgumentException("illegal remote path");
    	}
    	if ((permissions < 0) || (permissions > 999)) {
    		throw new IllegalArgumentException("illegal permissions " + permissions);
    	}
    	
    	return this.ftpClient.sendSiteCommand("chmod " + permissions + " " + remotePath);

    }
    
    public synchronized void close() throws IOException {
    	
    	this.ftpClient.logout();
    	
    }
    
    public synchronized void closeUponException() {
    	
		if (this.ftpClient.isConnected()) {
			try {
				this.ftpClient.disconnect();
			}
			catch(IOException ioe) {}
		}
		
    }
    
    public synchronized boolean delete(String remotePath) throws IOException {
    	
    	if (remotePath == null) {
    		throw new NullPointerException("illegal remote path");
    	}
    	if (remotePath.length() < 1) {
    		throw new IllegalArgumentException("illegal remote path");
    	}
    	
    	return this.ftpClient.deleteFile(remotePath);

    }
    
    public synchronized boolean exists(String remotePath) throws IOException {
    	
    	if (remotePath == null) {
    		throw new NullPointerException("illegal remote path");
    	}
    	if (remotePath.length() < 1) {
    		throw new IllegalArgumentException("illegal remote path");
    	}
    	
    	return this.ftpClient.sendSiteCommand("stat " + remotePath);
    	
    }
    
    public synchronized Map<String, Boolean> ls(String remotePath) throws IOException {
    	
    	if (remotePath == null) {
    		throw new NullPointerException("illegal remote path");
    	}
    	if (remotePath.length() < 1) {
    		throw new IllegalArgumentException("illegal remote path");
    	}
    	
    	final FTPFile[] files = this.ftpClient.listFiles(remotePath);
    	
    	final Map<String, Boolean> map = new TreeMap<String, Boolean>();
    	for (FTPFile file : files) {
    		if (file.getName().equals(".") || (file.getName().equals(".."))) continue; // skip . and ..

    		final Boolean storedIsDirectoryFlag = map.put(file.getName(), file.isDirectory());
    		if ((storedIsDirectoryFlag != null) && (storedIsDirectoryFlag != file.isDirectory())) {
    			throw new IOException("corrupted directory listing");
    		}
    	}
    	
    	return map;
    	
    }
    
    public synchronized boolean mkdir(String remotePath) throws IOException {

    	if (remotePath == null) {
    		throw new NullPointerException("illegal remote path");
    	}
    	if (remotePath.length() < 1) {
    		throw new IllegalArgumentException("illegal remote path");
    	}

    	return this.ftpClient.makeDirectory(remotePath);
    	
    }
    
    public synchronized void open() throws IOException {

		this.ftpClient.setDataTimeout(300000); // before connecting
		this.ftpClient.connect(this.ftpCredentials.getFTPServerURL());
		this.ftpClient.setSoTimeout(300000); // after connecting
		
	    this.ftpClient.login(this.ftpCredentials.getFTPServerLogin(), this.ftpCredentials.getFTPServerPassword());
	    this.ftpClient.enterLocalPassiveMode();
	    //this.ftpClient.setConnectMode(FTPConnectMode.PASV);
	    this.ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
	    //this.ftpClient.setType(FTPTransferType.BINARY);
	    final String ftpOpeningMsg = this.ftpClient.getReplyString();
    	System.out.println("\t" + ftpOpeningMsg + (ftpOpeningMsg.endsWith("\n") ? "" : "\n"));
    	//System.out.println("\t"+this.ftpClient.system());
    	
    	if (FTPReply.isPositiveCompletion(this.ftpClient.getReplyCode()) == false) {
            throw new IOException("FTP server refused connection, nothing uploaded");
    	}
    	
    }
    
    public synchronized boolean put(String localPath, String remotePath) throws IOException {
    	
    	if (localPath == null) {
    		throw new NullPointerException("illegal local path");
    	}
    	if (localPath.length() < 1) {
    		throw new IllegalArgumentException("illegal local path");
    	}
    	if (remotePath == null) {
    		throw new NullPointerException("illegal remote path");
    	}
    	if (remotePath.length() < 1) {
    		throw new IllegalArgumentException("illegal remote path");
    	}
    	
    	return this.ftpClient.storeFile(remotePath, new BufferedInputStream(new FileInputStream(localPath)));
    	
    }

    public synchronized boolean rmdir(String remotePath) throws IOException {
    	
    	if (remotePath == null) {
    		throw new NullPointerException("illegal remote path");
    	}
    	if (remotePath.length() < 1) {
    		throw new IllegalArgumentException("illegal remote path");
    	}
    	
    	return this.ftpClient.removeDirectory(remotePath);

    }
    
	// ****************************************************************
    
}
