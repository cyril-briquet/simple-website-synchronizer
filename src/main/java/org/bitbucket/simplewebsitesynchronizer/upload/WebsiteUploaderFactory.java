/*
 * Copyright 2009-2018 Cyril Briquet.
 * 
 * Licensed under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *  
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package org.bitbucket.simplewebsitesynchronizer.upload;

import java.util.Properties;

/** Website uploader factory.
 * @author Cyril Briquet
 */
public class WebsiteUploaderFactory {

	// ****************************************************************

	public WebsiteUploader createFTPWebsiteUploader(Properties props) {
	    
		if (props == null) {
			throw new NullPointerException("illegal properties");
		}
		
    	final FTPCredentials ftpCredentials = new FTPCredentialsFactory().createFTPCredentials(props);
    	
    	if (ftpCredentials.getFTPServerSecurity()) {
        	return new WebsiteUploader(ftpCredentials, new SFTPClient2(ftpCredentials));
    	}
    	else {
        	return new WebsiteUploader(ftpCredentials, new PlainFTPClient(ftpCredentials));
    	}
    	
	}
	
	// ****************************************************************

}
