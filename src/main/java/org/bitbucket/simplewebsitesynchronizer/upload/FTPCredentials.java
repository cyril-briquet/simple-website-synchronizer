/*
 * Copyright 2009-2018 Cyril Briquet.
 * 
 * Licensed under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *  
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package org.bitbucket.simplewebsitesynchronizer.upload;

import org.bitbucket.simplewebsitesynchronizer.FileUtils;

/** FTP credentials.
 * @author Cyril Briquet
 */
class FTPCredentials {

	private final String ftpServerUrl;
    private final String ftpServerLogin;
	private final String ftpServerPassword;
	private final String ftpServerSubdirectory;
	private final boolean ftpServerSecurity;

	// ****************************************************************

	protected FTPCredentials(String ftpServerUrl, String ftpServerLogin, String ftpServerPassword, String ftpServerSubdirectory,
							 boolean ftpServerSecurity) {

		if (ftpServerUrl == null) {
			throw new NullPointerException("illegal FTP server URL");
		}
		if (ftpServerUrl.length() < 1) {
			throw new IllegalArgumentException("illegal FTP server URL");
		}
		if (ftpServerLogin == null) {
			throw new NullPointerException("illegal FTP server login");
		}
		if (ftpServerLogin.length() < 1) {
			throw new IllegalArgumentException("illegal FTP server login");
		}
		if (ftpServerPassword == null) {
			throw new NullPointerException("illegal FTP server password");
		}
		/*if (ftpServerPassword.length() < 1) { -- alas, in practice, this could happen!
			throw new IllegalArgumentException("illegal FTP server password");
		}*/
		if (ftpServerSubdirectory == null) {
			throw new NullPointerException("illegal FTP server subdirectory");
		}
		/*if (ftpServerSubdirectory.length() < 1) {
			throw new IllegalArgumentException("illegal FTP server subdirectory");
		}*/

	    this.ftpServerUrl          = ftpServerUrl;
	    this.ftpServerLogin        = ftpServerLogin;
	    this.ftpServerPassword     = ftpServerPassword;
	    this.ftpServerSubdirectory =
	    	(ftpServerSubdirectory.isEmpty() ?
	    		ftpServerSubdirectory :
	    		(ftpServerSubdirectory.endsWith(FileUtils.CANONICAL_FILE_SEP) ?
	    			ftpServerSubdirectory :
	    			ftpServerSubdirectory + FileUtils.CANONICAL_FILE_SEP));
	    this.ftpServerSecurity     = ftpServerSecurity;
	    
	}
	
	// ****************************************************************

    public String getFTPServerURL() {

    	return this.ftpServerUrl;
	
    }

	public String getFTPServerLogin() {
	
		return this.ftpServerLogin;
	
	}

	public String getFTPServerPassword() {
		
		return this.ftpServerPassword;
	
	}

	public String getFTPServerSubdirectory() {

		return this.ftpServerSubdirectory;
	
	}
	
	public boolean getFTPServerSecurity() {
		
		return this.ftpServerSecurity;
		
	}
	
	// ****************************************************************

}
