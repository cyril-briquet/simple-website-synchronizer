/*
 * Copyright 2009-2018 Cyril Briquet.
 * 
 * Licensed under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *  
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package org.bitbucket.simplewebsitesynchronizer.upload;

import java.io.IOException;
import java.util.Map;

/** FTP agent.
 * @author Cyril Briquet
 */
interface FTPClientAdapter { // only what is required for simple-website-synchronizer package-level code

	// ****************************************************************

    public boolean chmod(String remotePath, int permissions) throws IOException;
    
    public void close() throws IOException; // protocol-level API: FTP logout(), SFTP close()
    
    public void closeUponException(); // socket-level API: FTP disconnect(), SFTP close()
    
    public boolean delete(String remotePath) throws IOException;

    public boolean exists(String remotePath) throws IOException;
    
    public Map<String, Boolean> ls(String remotePath) throws IOException;
    
    public boolean mkdir(String remotePath) throws IOException;
    
    public void open() throws IOException;
    
    public boolean put(String localPath, String remotePath) throws IOException;
    
    public boolean rmdir(String remotePath) throws IOException;
    
    // ****************************************************************

}
