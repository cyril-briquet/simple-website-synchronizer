/*
 * Copyright 2009-2018 Cyril Briquet.
 * 
 * Licensed under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *  
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package org.bitbucket.simplewebsitesynchronizer.upload;

import java.util.Properties;

/** FTP credentials factory.
 * @author Cyril Briquet
 */
class FTPCredentialsFactory {

	public FTPCredentials createFTPCredentials(Properties props) {

		if (props == null) {
			throw new NullPointerException("illegal Properties");
		}

		final String  ftpServerUrl          = props.getProperty("ftp-server-hostname");
		
	    final String  ftpServerLogin        = props.getProperty("ftp-server-login");
	    
	          String  ftpServerPassword     = props.getProperty("ftp-server-password");
	    if (ftpServerPassword == null) {
	    	System.out.print("\n\tEnter server password: ");
	        final char[] password = System.console().readPassword(); //read securely from console
	        ftpServerPassword = new String(password);
	    }
	    
	          String  ftpServerSubdirectory = props.getProperty("ftp-server-subdirectory");
	    if (ftpServerSubdirectory == null) ftpServerSubdirectory = ".";
	    
	          String protocol = props.getProperty("ftp-server-protocol");
	    if (protocol == null) {
	    	protocol = "ftp"; //throw new NullPointerException("illegal FTP server protocol");
		}
	    else {
	    	if ((protocol.equalsIgnoreCase("ftp") == false) && (protocol.equalsIgnoreCase("sftp") == false)) {
	    		throw new IllegalArgumentException("illegal FTP server protocol " + protocol);
	    	}
	    }
	    final boolean ftpServerSecurity = protocol.equalsIgnoreCase("sftp");

	    return new FTPCredentials(ftpServerUrl, ftpServerLogin, ftpServerPassword, ftpServerSubdirectory, ftpServerSecurity);
	    
	}

}
