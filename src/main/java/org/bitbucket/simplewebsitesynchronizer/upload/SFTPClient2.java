/*
 * Copyright 2009-2018 Cyril Briquet.
 * 
 * Licensed under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *  
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package org.bitbucket.simplewebsitesynchronizer.upload;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Hashtable;
import java.util.Map;
import java.util.TreeMap;
import java.util.Vector;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import com.jcraft.jsch.UserInfo;

/** SFTP implementation of the FTP agent interface.
 * <p>Relies on the <a href="https://github.com/mwiede/jsch/">JSch</a> library.</p>
 * @author Cyril Briquet
 */
class SFTPClient2 implements FTPClientAdapter {

	// original library: http://www.jcraft.com/jsch/
	// why fork: https://www.matez.de/index.php/2020/06/22/the-future-of-jsch-without-ssh-rsa/

	// see https://github.com/mwiede/jsch/blob/master/examples/Sftp.java for an example
	
    private final FTPCredentials ftpCredentials;
    
    private       JSch jsch;
    private       Session session;
    private       ChannelSftp c;

    // ****************************************************************
	
	public SFTPClient2(FTPCredentials ftpCredentials) {
		
		if (ftpCredentials == null) {
			throw new NullPointerException("illegal FTP credentials");
		}
		
		this.ftpCredentials = ftpCredentials;
		
		this.jsch = null;
		this.session = null;
		this.c = null;
		
	}
	
	// ****************************************************************
	
    public synchronized boolean chmod(String remotePath, int permissions) throws IOException {
    	
    	if (remotePath == null) {
    		throw new NullPointerException("illegal remote path");
    	}
    	if (remotePath.length() < 1) {
    		throw new IllegalArgumentException("illegal remote path");
    	}
    	if ((permissions < 0) || (permissions > 999)) {
    		throw new IllegalArgumentException("illegal permissions " + permissions);
    	}
    	
    	try {
    		final int octalPermissions = Integer.parseInt(Integer.toString(permissions), 8); // must convert to octal, not very practical
    		
    		this.c.chmod(octalPermissions, remotePath);
    	}
    	catch (SftpException e) {
			return false;
		}
    	
    	return true;
    	
    }
    
    public synchronized void close() throws IOException {

		this.session.disconnect();
    	
    }
    
    public synchronized void closeUponException() {
    	
   		if (this.session != null) this.session.disconnect();
    	
    }
    
    public synchronized boolean delete(String remotePath) throws IOException {
    	
    	if (remotePath == null) {
    		throw new NullPointerException("illegal remote path");
    	}
    	if (remotePath.length() < 1) {
    		throw new IllegalArgumentException("illegal remote path");
    	}
    	
    	try {
    		this.c.rm(remotePath);
    	}
    	catch (SftpException e) {
    		return false;
    	}
    	
    	return true;
    	
    }
    
    public synchronized boolean exists(String remotePath) throws IOException {
    	
    	if (remotePath == null) {
    		throw new NullPointerException("illegal remote path");
    	}
    	if (remotePath.length() < 1) {
    		throw new IllegalArgumentException("illegal remote path");
    	}
    	
    	try {
    		this.c.stat(remotePath);
    		return true;
    	}
    	catch (SftpException e) {
    		return false; // file or directory does not exist
    	}
    	
    }
    
    public synchronized Map<String, Boolean> ls(String remotePath) throws IOException {
    	
    	if (remotePath == null) {
    		throw new NullPointerException("illegal remote path");
    	}
    	if (remotePath.length() < 1) {
    		throw new IllegalArgumentException("illegal remote path");
    	}
    	
    	if (exists(remotePath) == false) {
    		return new TreeMap<String, Boolean>(); // directory does not exist
    	}

    	final Map<String, Boolean> map = new TreeMap<String, Boolean>();

    	try {
	    	Vector<ChannelSftp.LsEntry> files = this.c.ls(remotePath);
	    	
	    	for (ChannelSftp.LsEntry file : files) {
	    		if (file.getLongname().equals(".") || (file.getLongname().equals(".."))) continue; // skip . and ..
	    		
	    		final Boolean storedIsDirectoryFlag = map.put(file.getLongname(), file.getAttrs().isDir());
	    		if ((storedIsDirectoryFlag != null) && (storedIsDirectoryFlag != file.getAttrs().isDir())) {
	    			throw new IOException("corrupted directory listing");
	    		}
	    	}
    	}
    	catch (SftpException e) {
    		return map;
    	}
	    	
    	return map;
    	
    }
    
    public synchronized boolean mkdir(String remotePath) throws IOException {
    	
    	if (remotePath == null) {
    		throw new NullPointerException("illegal remote path");
    	}
    	if (remotePath.length() < 1) {
    		throw new IllegalArgumentException("illegal remote path");
    	}
    	
    	try {
    		this.c.mkdir(remotePath);
    	}
    	catch (SftpException e) {
    		return false;
    	}
    	
    	return true;
    	
    }
    
    private final class MyUserInfo implements UserInfo {
    	  
    	private MyUserInfo() {
    		
    	}

		public boolean promptYesNo(String s) {
			
			try {
				final InputStreamReader br = new InputStreamReader(System.in, "UTF-8");
	        
				for ( ; ; ) {
					System.out.print(s + " (y/n then ENTER): ");
					final char c = (char) br.read();
					if ((c == 'y') || (c == 'Y')) {
						return true;
					}
					else if ((c == 'n') || (c == 'N')) {
						return false;
					}
					else {
						continue;
					}
				}
			}
			catch (IOException e) {
				throw new IllegalStateException(e.getMessage(), e);
			}
	        
		}

		public boolean promptPassword(String s) {
			
			return true;
			
		}

		public String getPassword() {
			  
			return SFTPClient2.this.ftpCredentials.getFTPServerPassword();
			  
		}
		  
		public boolean promptPassphrase(String s) {
		
			System.out.println("MyUserInfo: " + s);
		
			throw new UnsupportedOperationException("promptPassphrase(): unsupported operation");
		
		}

		public String getPassphrase() {
			  
			throw new UnsupportedOperationException("getPassphrase(): unsupported operation");
			  
		}

		public void showMessage(String s) {
		
			System.out.println("MyUserInfo: " + s);
		
			throw new UnsupportedOperationException("showMessage(): unsupported operation");
		
		}
		  
	}
    
    public synchronized void open() throws IOException {

    	try {
    		final String enableLoggingFlag = System.getProperty("org.bitbucket.simplewebsitesynchronizer.enableLogging");
    		if ("true".equals(enableLoggingFlag)) {
    			JSch.setLogger(new MyLogger());
    		}
    	      
    		this.jsch = new JSch();
    		
    		setKnownHosts(this.jsch);
    		
    		this.session = this.jsch.getSession(this.ftpCredentials.getFTPServerLogin(), this.ftpCredentials.getFTPServerURL(), 22);
    		this.session.setUserInfo(new MyUserInfo());
    		this.session.connect();

    		final Channel channel = this.session.openChannel("sftp");
    		channel.connect();
    		this.c = (ChannelSftp) channel;    		
    		
    	}
    	catch (JSchException e) {
    		throw new IOException(e.getMessage(), e);
    	}
    	
    }
    
    private static void setKnownHosts(JSch jsch) throws JSchException {
    	
    	if (jsch == null) {
    		throw new NullPointerException("illegal JSch");
    	}
    	
    	String userHomeDirectory = System.getProperty("user.home");
    	if (userHomeDirectory.endsWith(File.separator) == false) {
    		userHomeDirectory += File.separator;
    	}
    	
    	final String knownHostsPath = userHomeDirectory + ".ssh" + File.separator + "known_hosts";
    	// e.g. "~/.ssh/known_hosts"
    	
		final Path path = Paths.get(knownHostsPath);
		if (Files.exists(path)) {
			jsch.setKnownHosts(knownHostsPath);
		}
    	
    }
    
    public synchronized boolean put(String localPath, String remotePath) throws IOException {
    	
    	if (localPath == null) {
    		throw new NullPointerException("illegal local path");
    	}
    	if (localPath.length() < 1) {
    		throw new IllegalArgumentException("illegal local path");
    	}
    	if (remotePath == null) {
    		throw new NullPointerException("illegal remote path");
    	}
    	if (remotePath.length() < 1) {
    		throw new IllegalArgumentException("illegal remote path");
    	}
    	
    	try {
            this.c.put(new BufferedInputStream(new FileInputStream(localPath)), remotePath);
    	}
    	catch (SftpException e) {
    		return false;
    	}
    	
    	return true;
    	
    }
    
    public synchronized boolean rmdir(String remotePath) throws IOException {
    	
    	if (remotePath == null) {
    		throw new NullPointerException("illegal remote path");
    	}
    	if (remotePath.length() < 1) {
    		throw new IllegalArgumentException("illegal remote path");
    	}
    	
    	try {
    		this.c.rmdir(remotePath);
    	}
    	catch (SftpException e) {
    		return false;
    	}
    	
    	return true;
    	
    }
    
    // ****************************************************************

    public static class MyLogger implements com.jcraft.jsch.Logger {

    	private static Map<Integer, String> LEVELS = new Hashtable<Integer, String>();
    	
        static{
          LEVELS.put(Integer.valueOf(DEBUG), "DEBUG: ");
          LEVELS.put(Integer.valueOf(INFO), "INFO: ");
          LEVELS.put(Integer.valueOf(WARN), "WARN: ");
          LEVELS.put(Integer.valueOf(ERROR), "ERROR: ");
          LEVELS.put(Integer.valueOf(FATAL), "FATAL: ");
        }
        
        public boolean isEnabled(int level){
          return true;
        }
        
        public void log(int level, String message){
          System.err.print(LEVELS.get(level));
          System.err.println(message);
        }
        
      }

    // ****************************************************************

}
