/*
 * Copyright 2009-2018 Cyril Briquet.
 * 
 * Licensed under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *  
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package org.bitbucket.simplewebsitesynchronizer.upload;

import org.bitbucket.simplewebsitesynchronizer.FileUtils;
import org.bitbucket.simplewebsitesynchronizer.state.FileMetadata;
import org.bitbucket.simplewebsitesynchronizer.state.FileState;
import org.bitbucket.simplewebsitesynchronizer.state.WebsiteState;

import java.io.IOException;
import java.util.Map;

/** Website uploader.
 * @author Cyril Briquet
 */
public class WebsiteUploader {

	private final FTPCredentials ftpCredentials;
    private final FTPClientAdapter ftpClient;

	// ****************************************************************

	WebsiteUploader(FTPCredentials ftpCredentials, FTPClientAdapter ftpClient) {
		
		if (ftpCredentials == null) {
			throw new NullPointerException("illegal FTP credentials");
		}
		if (ftpClient == null) {
			throw new NullPointerException("illegal FTP client");
		}

		this.ftpCredentials = ftpCredentials;
		this.ftpClient = ftpClient;
		
	}
	
	// ****************************************************************

    public synchronized void upload(WebsiteState state) throws IOException {

    	if (state == null) {
    		throw new NullPointerException("null state");
    	}
    	
    	try {
    		connect();
	
	    	for (FileMetadata fileMetadata : state) {
				switch (fileMetadata.getFileState()) {
				case synced:
					continue;
					
				case to_upload:
		    		final String localPath  = state.getWebsiteBaseDirectory() + fileMetadata.getFileSignature().getFilePath();
		    		final String remotePath = this.ftpCredentials.getFTPServerSubdirectory() + fileMetadata.getFileSignature().getFilePath();
		    		
		    		if (upload(localPath, remotePath)) {
		    			fileMetadata.setFileState(FileState.synced);
		    			state.persist();
		    		}
		    		break;
		    		
				case to_delete:
		    		final String obsoleteLocalPath  = state.getWebsiteBaseDirectory() + fileMetadata.getFileSignature().getFilePath();
		    		final String obsoleteRemotePath = this.ftpCredentials.getFTPServerSubdirectory() + fileMetadata.getFileSignature().getFilePath();

		    		if (delete(obsoleteLocalPath, obsoleteRemotePath)) {
			    		fileMetadata.setFileState(FileState.deleted);
		    			state.persist();
		    		}
					break;
					
				default:
					throw new IllegalStateException("unsupported file state " + fileMetadata.getFileState());
				}
	    	}
	
	    	disconnect();
    	}
    	catch (IOException e) {
    		this.ftpClient.closeUponException();
    		throw e;
    	}
    	
    }

	// ****************************************************************

    private synchronized void connect() throws IOException {

	    // FTPClient is NOT thread safe: synchronize code to use with multiple threads
    	System.out.println("\n\tOpening " + (this.ftpCredentials.getFTPServerSecurity() ? "SFTP" : "FTP") +
    					   " connection to " + this.ftpCredentials.getFTPServerURL() + "...");

    	this.ftpClient.open();

    }
    
    private synchronized void disconnect() throws IOException {
    	
    	System.out.println("\tClosing " + (this.ftpCredentials.getFTPServerSecurity() ? "SFTP" : "FTP") +
    					   " connection to " + this.ftpCredentials.getFTPServerURL() + "...");
    	
    	this.ftpClient.close();

    }
    
    private synchronized boolean upload(String localPath, String remotePath) {

    	if (localPath == null) {
    		throw new NullPointerException("null local path");
    	}
    	if (remotePath == null) {
    		throw new NullPointerException("null remote path");
    	}
    	
		boolean completedUpload = false;
		
		try {
	    	System.out.println("\tUploading " + localPath + " to " + remotePath + "...");
	    	
			if (localPath.endsWith(FileUtils.CANONICAL_FILE_SEP)) {
				final int permissions = 705;
				if ((this.ftpClient.exists(remotePath) == true) && // precondition to chmod
					(this.ftpClient.chmod(remotePath, permissions) == false)) {
					System.out.println("\tWarning: cannot chmod " + permissions + " remote directory " + remotePath + " before uploading");
				}
				if ((this.ftpClient.exists(remotePath) == false) && // precondition to mkdir
					(this.ftpClient.mkdir(remotePath) == false)) {
					throw new IOException("cannot create remote directory " + remotePath);
				}
				if (this.ftpClient.chmod(remotePath, permissions) == false) {
					System.out.println("\tWarning: cannot chmod " + permissions + " remote directory " + remotePath);
				}
			}
			else {
				final int permissions = 604;
				if ((this.ftpClient.exists(remotePath) == true) && // precondition to chmod
					(this.ftpClient.chmod(remotePath, permissions) == false)) {
					System.out.println("\tWarning: cannot chmod " + permissions + " remote file " + remotePath + " before uploading");
				}
			    //this.ftpClient.put(localPath, remotePath);
				if (this.ftpClient.put(localPath, remotePath) == false) {
					throw new IOException("cannot upload file " + remotePath);
				}
				if (this.ftpClient.chmod(remotePath, permissions) == false) {
					System.out.println("\tWarning: cannot chmod " + permissions + " remote file " + remotePath);
				}
			}
			
	    	completedUpload = true;
		}
		catch (IOException e) {
		    System.out.println("======> ** " + e + " **");
		    //e.printStackTrace(); //debugging code
		}

		return completedUpload;

    }
    
    private synchronized boolean delete(String localPath, String remotePath) {

    	if (localPath == null) {
    		throw new NullPointerException("null local path");
    	}
    	if (remotePath == null) {
    		throw new NullPointerException("null remote path");
    	}
    	
		boolean completedUpload = false;

		try {
	    	System.out.println("\tDeleting " + remotePath + "...");

	    	if (localPath.endsWith(FileUtils.CANONICAL_FILE_SEP)) {
	    		deleteDirectory(remotePath);
			}
			else {
				if (this.ftpClient.delete(remotePath) == false) {
				    System.out.println("\t** cannot delete " + remotePath + " file **");
				}
			}

	    	completedUpload = true;
		}
		catch (IOException e) {
		    System.out.println("======> ** " + e + " **");
		    //e.printStackTrace(); //debugging code
		}
    	
		return completedUpload;

    }
    
    private synchronized void deleteDirectory(String remotePath) throws IOException {

    	if (remotePath == null) {
    		throw new NullPointerException("null remote path");
    	}

    	final Map<String, Boolean> files = this.ftpClient.ls(remotePath);
    	for (Map.Entry<String, Boolean> entry : files.entrySet()) {
    		String filename = remotePath + FileUtils.CANONICAL_FILE_SEP + entry.getKey();
    		final boolean isDirectory = entry.getValue();
    		
    		if (isDirectory) {
    			deleteDirectory(filename);
    		}
    		else {
				if (this.ftpClient.delete(filename) == false) {
				    System.out.println("\t** cannot delete " + filename + " file **");
				}
    		}
    	}
    	
    	if (this.ftpClient.rmdir(remotePath) == false) {
		    System.out.println("\t** cannot delete " + remotePath + "  directory **");
    	}

    }

	// ****************************************************************
    
}
