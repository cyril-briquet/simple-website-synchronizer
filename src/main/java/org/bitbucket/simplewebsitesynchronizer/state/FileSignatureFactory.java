/*
 * Copyright 2009-2018 Cyril Briquet.
 * 
 * Licensed under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *  
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package org.bitbucket.simplewebsitesynchronizer.state;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.bitbucket.simplewebsitesynchronizer.FileUtils;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

/** File signature factory.
 * @author Cyril Briquet
 */
public class FileSignatureFactory {

	private final String websiteBaseDirectory;

	// ****************************************************************
	
	public FileSignatureFactory(String websiteBaseDirectory) {

		if (websiteBaseDirectory == null) {
			throw new NullPointerException("illegal website base directory");
		}
		
		this.websiteBaseDirectory = websiteBaseDirectory;
		
	}
	
	// ****************************************************************

	public FileSignature createFileSignature(String filePath) throws IOException {
		
		if (filePath == null) {
			throw new NullPointerException("null file path");
		}
		if (filePath.endsWith(FileUtils.CANONICAL_FILE_SEP)) {
			throw new IllegalArgumentException("file is a directory");
		}
		
		final byte[] rawFileContents = readBinaryFile(filePath);
		final byte[] rawDigitalSignature = computeChecksum(rawFileContents);
		
		return createFileSignature(filePath,byteArrayToHexStringArray(rawDigitalSignature));
		
	}

	public FileSignature createDirectorySignature(String filePath) {
		
		if (filePath == null) {
			throw new NullPointerException("null file path");
		}

		return createFileSignature(filePath, "no-digital-signature");
		
	}

	public FileSignature createFileSignature(String filePath, String digitalSignature) {

		if (filePath.startsWith(this.websiteBaseDirectory)) {
			filePath = filePath.substring(this.websiteBaseDirectory.length());
		}

		return new FileSignature(filePath, digitalSignature);	
		
	}

	// ****************************************************************
	
    /** Reads binary file contents of the specified file.
	 * @param fn filename
	 * @return read file (binary data)
	 */
	private static byte[] readBinaryFile(String fn) throws IOException {
	
    	if (fn == null) {
    		throw new NullPointerException("illegal filename");
    	}

		InputStream is = null;
		byte[] data = new byte[0]; 
		try {
			is = new BufferedInputStream(new FileInputStream(fn));
			final int buflen = 1024 * 1024;
			final byte[] buf = new byte[buflen];
			byte[] tmp = null;
			int len = 0;
			while ((len = is.read(buf,0,buflen)) != -1) {
				tmp = new byte[data.length+len];
	            System.arraycopy(data, 0, tmp, 0,data.length);
	            System.arraycopy(buf, 0, tmp, data.length, len);
	            data = tmp;
	            tmp = null;            
			}
		}
		finally {
	         if (is != null) {
	        	 try {
	        		 is.close();
	        	 }
	        	 catch (Exception e) {}
	         }
		}

		return data; 
		
	}
	
	private static byte[] computeChecksum(byte[] data) {

		if (data == null) {
			throw new NullPointerException("illegal data");
		}
		
		try {
			final MessageDigest md = MessageDigest.getInstance("SHA-256");
			return md.digest(data);
		}
		catch (NoSuchAlgorithmException e) {
			throw new IllegalStateException(e);
		}
		
	}

	@SuppressFBWarnings("DM_CONVERT_CASE")
	private static String byteToHexString(byte b) {

		final StringBuilder sb = new StringBuilder();

		final int i = b + (2 << 7);
		sb.append("0x").append(Integer.toHexString(i).toUpperCase());
		
		return sb.toString();		
		
	}

	private static String byteArrayToHexStringArray(byte[] b) {
		
		if (b == null) {
			throw new NullPointerException("illegal byte array");
		}

		final StringBuilder sbDigitalSignature = new StringBuilder();
		
		for (int i = 0 ; i < b.length ; ++i) {
			sbDigitalSignature.append(byteToHexString(b[i]));
			if (i < b.length - 1) {
				sbDigitalSignature.append(",");
			}
		}
		
		return sbDigitalSignature.toString();

	}

}
