/*
 * Copyright 2009-2018 Cyril Briquet.
 * 
 * Licensed under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *  
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package org.bitbucket.simplewebsitesynchronizer.state;

import static org.bitbucket.simplewebsitesynchronizer.FileUtils.canonicalizeFileSeparators;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TreeMap;

import org.bitbucket.simplewebsitesynchronizer.sitemap.SitemapTimestamp;

/** Website state loader.
 * @author Cyril Briquet
 */
class WebsiteStateMapLoader {

    private final FileSignatureFactory fileSignatureFactory;
    private final Map<String, Boolean> ignoredFiles;
    
	// ****************************************************************

	public WebsiteStateMapLoader(FileSignatureFactory fileSignatureFactory, Map<String, Boolean> ignoredFiles) {
		
		if (fileSignatureFactory == null) {
			throw new NullPointerException("illegal file signature factory");
		}
		if (ignoredFiles == null) {
			throw new NullPointerException("illegal ignored files");
		}

		this.fileSignatureFactory = fileSignatureFactory;
		this.ignoredFiles = ignoredFiles;
		
	}
	
	// ****************************************************************

    public Map<String, FileMetadata> loadPersistedWebsiteStateMap(String websiteBaseDirectory, String persistedStatePath) throws IOException {
    	
    	if (websiteBaseDirectory == null) {
    		throw new NullPointerException("null website base directory");
    	}
    	if (persistedStatePath == null) {
    		throw new NullPointerException("null persisted state path");
    	}

    	final String path = websiteBaseDirectory + persistedStatePath;

		System.out.println("\n\tReading previous website state from " + path + "...");

		final Map<String, FileMetadata> persistedDirectoryMap = new TreeMap<String, FileMetadata>();

		BufferedReader br = null;		
		try {
    		br = Files.newBufferedReader(FileSystems.getDefault().getPath(path));
    	}
    	catch (IOException e) {
	    	System.out.println("\tNo previously persisted state found");
    	}
	    try {
	        if (br != null) {
				String line = br.readLine();
		        while (line != null) {
		        	line = line.trim();
		        	if ((line.length() > 0) && (line.startsWith("#") == false)) {
		        		processLine(line, persistedDirectoryMap);
		        	}
		            line = br.readLine();
		        }
	    	}
	    }
	    finally  {
	    	if (br != null) {
	    		br.close();
	    	}
	    }

	    return persistedDirectoryMap;
    	
    }

	// ****************************************************************

    private void processLine(String line, Map<String, FileMetadata> persistedDirectoryMap) throws IOException {

    	if (line == null) {
    		throw new NullPointerException("null line");
    	}
    	if (persistedDirectoryMap == null) {
    		throw new NullPointerException("null persisted directory map");
    	}
    	
    	final StringTokenizer st = new StringTokenizer(line, " ");
    	if (st.countTokens() != 4) {
	    	throw new IOException(st.countTokens() + " tokens found within line \"" + line + "\", 4 expected ");
	    }
	    
	    final FileState fileState = FileState.valueOf(st.nextToken());
	    final SitemapTimestamp sitemapTimestamp = new SitemapTimestamp(st.nextToken());
	    final String digitalSignature = st.nextToken();
	    final String filePath = canonicalizeFileSeparators(st.nextToken());
	    if (this.ignoredFiles.get(filePath) != null) {
			System.out.println("\tIgnored file " + filePath + " from previous state");
	    	return;
	    }
	    final FileSignature fileSignature = this.fileSignatureFactory.createFileSignature(filePath, digitalSignature);

	    final FileMetadata fileMetadata = new FileMetadata(fileState, sitemapTimestamp, fileSignature);
	    if (persistedDirectoryMap.put(filePath, fileMetadata) != null) {
	    	throw new IOException("file (or directory) " + filePath + " referenced more than once, please fix previously persisted state");
	    }

    }
    
	// ****************************************************************

}
