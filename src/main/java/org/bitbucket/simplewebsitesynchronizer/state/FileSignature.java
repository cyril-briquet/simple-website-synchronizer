/*
 * Copyright 2009-2018 Cyril Briquet.
 * 
 * Licensed under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *  
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package org.bitbucket.simplewebsitesynchronizer.state;

/** File signature.
 * @author Cyril Briquet
 */
public class FileSignature {

	private final String filePath;
	private final String digitalSignature;
	private final String stringRepresentation;

	// ****************************************************************

	protected FileSignature(String filePath, String digitalSignature) {
		
		if (filePath == null) {
			throw new NullPointerException("null file path");
		}
		if (filePath.length() < 1) {
			throw new IllegalArgumentException("illegal file path");
		}
		if (digitalSignature == null) {
			throw new NullPointerException("null digital signature");
		}
		if (digitalSignature.length() < 1) {
			throw new IllegalArgumentException("illegal digital signature");
		}

		this.filePath = filePath;
		this.digitalSignature = digitalSignature;

    	final StringBuilder sb = new StringBuilder();
    	sb.append(this.digitalSignature).append(" ").append(this.filePath);
		this.stringRepresentation = sb.toString(); 
		
	}
	
	// ****************************************************************

    @Override
	public String toString() {

    	return this.stringRepresentation;

    }

	// ****************************************************************

	public int compareTo(FileSignature other) {
		
		return this.toString().compareTo(other.toString());
		
	}

	@Override
	public boolean equals(Object obj) {
		
		if (obj instanceof FileSignature == false) {
			return false;
		}
		
		return (this.compareTo((FileSignature) obj) == 0);
		
	}
	
	@Override
	public int hashCode() {
	
		return this.stringRepresentation.hashCode();
	
	}
    
	// ****************************************************************

	public String getFilePath() {
		
		return this.filePath;
		
	}

	public String getDigitalSignature() {
		
		return this.digitalSignature;
		
	}
	
}
