/*
 * Copyright 2009-2018 Cyril Briquet.
 * 
 * Licensed under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *  
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package org.bitbucket.simplewebsitesynchronizer.state;

import static org.bitbucket.simplewebsitesynchronizer.FileUtils.canonicalizeFileSeparators;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

import org.bitbucket.simplewebsitesynchronizer.FileUtils;

/** Directory mapper. Recursively maps the directory's files to their digital signatures.
 * @author Cyril Briquet
 */
class DirectoryMapper {

    private final FileSignatureFactory fileSignatureFactory;
    private final String persistedStatePath;
    private final Map<String, Boolean> ignoredFiles;

	// ****************************************************************

	public DirectoryMapper(FileSignatureFactory fileSignatureFactory, String persistedStatePath, Map<String,Boolean> ignoredFiles) {

		if (fileSignatureFactory == null) {
			throw new NullPointerException("illegal file signature factory");
		}
		if (persistedStatePath == null) {
			throw new NullPointerException("illegal persisted state path");
		}
		if (ignoredFiles == null) {
			throw new NullPointerException("illegal ignored files");
		}

		this.fileSignatureFactory = fileSignatureFactory;
		this.persistedStatePath = persistedStatePath;
		this.ignoredFiles = ignoredFiles;
		
	}

	// ****************************************************************

    /** Recursively maps the directory's files to their digital signatures.
     * @param dir base directory to map
     * @return recursive map of the directory's files to their digital signatures; 
     * keys = filenames with complete paths, ending with a path separator, e.g. /, if the file is actually a directory;
     * values = digital signatures
     * @throws IOException if an error is encountered while recursively mapping the directory's files
     */
    public Map<String, FileSignature> mapDirectory(String dir) throws IOException {

    	if (dir == null) {
    		throw new NullPointerException("null directory");
    	}

		if (dir.endsWith(FileUtils.CANONICAL_FILE_SEP) == false) {
		    dir += FileUtils.CANONICAL_FILE_SEP;
		}

		final File f = new File(dir);
	
		if (f.isDirectory() == false) {
			throw new IllegalArgumentException("directory " + dir + " is not a directory");
		}

		final Map<String, FileSignature> directoryFilesMap = new TreeMap<String,FileSignature>();

		final String[] directoryListing = f.list();
		if (directoryListing == null) {
			throw new IllegalArgumentException("directory " + dir + " is not a directory");
		}
	    for (int i = 0 ; i < directoryListing.length ; ++i) {
	    	final String filePath = canonicalizeFileSeparators(dir + directoryListing[i]);
	    	if (excludeFile(filePath)) continue;
	    	
	    	if (new File(filePath).isDirectory()) {
	    		final String dirPath = filePath + FileUtils.CANONICAL_FILE_SEP;
	    		final FileSignature dirSignature = this.fileSignatureFactory.createDirectorySignature(dirPath);
			    directoryFilesMap.put(dirSignature.getFilePath(),dirSignature);
			    System.out.println("\tAdded " + dirPath + " as " + dirSignature.getFilePath());
			    directoryFilesMap.putAll(mapDirectory(dirPath));
		    }
		    else {
		    	final FileSignature fileSignature = this.fileSignatureFactory.createFileSignature(filePath);
			    directoryFilesMap.put(fileSignature.getFilePath(), fileSignature);
			    System.out.println("\tAdded " + filePath + " as " + fileSignature.getFilePath());
			}
	    }

	    System.out.println("\tProcessed "+dir);

        return directoryFilesMap;

    }
    
    private boolean excludeFile(String filePath) {
    	
    	if (filePath == null) {
    		throw new NullPointerException("null file path");
    	}
    	
    	if (filePath.contains(" ")) {
			throw new IllegalArgumentException("\n\tUnsupported filename " + filePath + " contains whitespace, please rename to remove spacing characters");
    	}
    	if (filePath.contains(".svn/") || filePath.endsWith(".svn")) {
    		return true;
    	}
    	if (filePath.endsWith("~")) {
    		return true;
    	}
    	if (filePath.endsWith(".DS_Store")) {
    		return true;
    	}
    	if (filePath.endsWith(this.persistedStatePath)) {
    		return true;
    	}
    	if (this.ignoredFiles.get(filePath) != null) {
			System.out.println("\tIgnored file "+filePath);
    		return true;
    	}
    	for (Map.Entry<String, Boolean> entry : this.ignoredFiles.entrySet()) {
    		final String ignoredFile = entry.getKey();
    		if (filePath.startsWith(ignoredFile) && (Boolean.TRUE.equals(this.ignoredFiles.get(ignoredFile)))) {
    			return true;
    		}
    	}
    	
    	return false;
    	
    }
        
}
