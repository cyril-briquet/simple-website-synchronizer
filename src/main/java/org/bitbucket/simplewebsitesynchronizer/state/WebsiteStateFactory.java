/*
 * Copyright 2009-2018 Cyril Briquet.
 * 
 * Licensed under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *  
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package org.bitbucket.simplewebsitesynchronizer.state;

import java.io.IOException;
import java.util.Map;

import org.bitbucket.simplewebsitesynchronizer.sitemap.SitemapTimestamp;

/** Website state factory.
 * @author Cyril Briquet
 */
public class WebsiteStateFactory {

    private final FileSignatureFactory fileSignatureFactory;

	// ****************************************************************

	public WebsiteStateFactory(FileSignatureFactory fileSignatureFactory) {
		
		if (fileSignatureFactory == null) {
			throw new NullPointerException("illegal file signature factory");
		}
		
		this.fileSignatureFactory = fileSignatureFactory;
		
	}

	// ****************************************************************

	/** Creates a model of the current website state.
     * @param websiteBaseDirectory website base directory
     * @param persistedStatePath persisted website state path
     * @param ignoredFiles set of files to ignore
     * @param unmapped_files set of files to not map
     * @return current website state (from disk and from previously persisted website state)
	 * @throws IOException if an error is encountered while creating website state
     */
    public WebsiteState createWebsiteState(String websiteBaseDirectory, String persistedStatePath, Map<String, Boolean> ignoredFiles) throws IOException {

    	if (websiteBaseDirectory == null) {
    		throw new NullPointerException("null website base directory");
    	}
    	if (persistedStatePath == null) {
    		throw new NullPointerException("null persisted state path");
    	}
		if (ignoredFiles == null) {
			throw new NullPointerException("illegal ignored files");
		}

	    System.out.println("\n\tMapping website files to digital signatures, from disk...");
		System.out.println("\tWebsite base directory is: " + websiteBaseDirectory);
	    final DirectoryMapper directoryMapper = new DirectoryMapper(this.fileSignatureFactory, persistedStatePath, ignoredFiles);
		final Map<String, FileSignature> directoryMap = directoryMapper.mapDirectory(websiteBaseDirectory);

		final WebsiteStateMapLoader websiteStateMapLoader = new WebsiteStateMapLoader(this.fileSignatureFactory, ignoredFiles);
		final Map<String, FileMetadata> previouslyPersistedStateMap =
			websiteStateMapLoader.loadPersistedWebsiteStateMap(websiteBaseDirectory, persistedStatePath);
		
		final SitemapTimestamp timestamp = new SitemapTimestamp();
		
		updateWebsiteStateMap(previouslyPersistedStateMap, directoryMap, timestamp);
		
	    return new WebsiteState(websiteBaseDirectory, persistedStatePath, previouslyPersistedStateMap, timestamp);

    }
    
	// ****************************************************************

    private void updateWebsiteStateMap(Map<String,FileMetadata> previouslyPersistedState, Map<String, FileSignature> directoryMap,
    								   SitemapTimestamp timestamp) {
    	
    	if (previouslyPersistedState == null) {
    		throw new NullPointerException("null previously persisted state");
    	}
    	if (directoryMap == null) {
    		throw new NullPointerException("null directory map");
    	}
    	if (timestamp == null) {
    		throw new NullPointerException("illegal timestamp");
    	}
    	
		System.out.println("\n\tUpdating website state...");

    	// update current website state from previously-persisted website state, remove previously-persisted files from directory map
		for (Map.Entry<String, FileMetadata> persistedFile : previouslyPersistedState.entrySet()) {
			final String persistedFilePath = persistedFile.getKey();
			final FileMetadata persistedFileMetadata = persistedFile.getValue();
			final FileSignature currentFileSignature = directoryMap.remove(persistedFilePath);

			// determine current file state
			final FileMetadata currentFileMetadata = (currentFileSignature == null) ?
				// file not found on disk: make it obsolete
				updateObsoleteFileState(persistedFilePath, persistedFileMetadata) :
				// file present on disk: update state if file has been updated
				updateFileState(persistedFilePath, persistedFileMetadata, currentFileSignature, timestamp);
			
			// update current website state with current file state
			persistedFile.setValue(currentFileMetadata);
			// instead of: previouslyPersistedState.put(persistedFilePath, currentFileMetadata);
			//             see Map.entrySet() Javadoc on concurrent modifications
		}

		// add new files (i.e. those remaining in the directory map after updating the previously-persisted website state) to current website state
		for (Map.Entry<String, FileSignature> newFile : directoryMap.entrySet()) {
			final String newFilePath = newFile.getKey();
			final FileSignature newFileSignature = newFile.getValue();
			
			final FileMetadata newFileMetadata = new FileMetadata(FileState.to_upload, timestamp, newFileSignature);
			previouslyPersistedState.put(newFilePath, newFileMetadata);
		}
    	
    }

    private FileMetadata updateObsoleteFileState(String persistedFilePath, FileMetadata persistedFileMetadata) {

    	if (persistedFilePath == null) {
    		throw new NullPointerException("illegal persisted file path");
    	}
    	if (persistedFileMetadata == null) {
    		throw new NullPointerException("illegal persisted file metadata");
    	}
		
		System.out.println("======> ** File " + persistedFilePath + " not found (to delete from FTP server) **");
		return new FileMetadata(FileState.to_delete, persistedFileMetadata.getFileTimestamp(), persistedFileMetadata.getFileSignature());

    }
    
    private FileMetadata updateFileState(String persistedFilePath, FileMetadata persistedFileMetadata, FileSignature currentFileSignature,
    									 SitemapTimestamp timestamp) {

    	if (persistedFilePath == null) {
    		throw new NullPointerException("illegal persisted file path");
    	}
    	if (persistedFileMetadata == null) {
    		throw new NullPointerException("illegal persisted file metadata");
    	}
    	if (currentFileSignature == null) {
    		throw new NullPointerException("illegal current file signature");
    	}
    	if (timestamp == null) {
    		throw new NullPointerException("illegal timestamp");
    	}
    	
		final FileMetadata currentFileMetadata = persistedFileMetadata;
		
		// compare the persisted signature vs. the current signature
		if (persistedFileMetadata.getFileSignature().equals(currentFileSignature) == false) { // updated file
			if (persistedFileMetadata.getFileState() == FileState.to_delete) {
				System.out.println("\tFile " + persistedFilePath + " back into website directory");
			}
			System.out.println("\tFile " + persistedFilePath + " updated (to upload to FTP server)");
			return new FileMetadata(FileState.to_upload, timestamp, currentFileSignature);
		}
		else { // unchanged file
			switch (persistedFileMetadata.getFileState()) {
			case to_upload:
			case synced:
				break;
			case to_delete:
				System.out.println("\tFile " + persistedFilePath + " back into website directory");
				return new FileMetadata(FileState.synced, persistedFileMetadata.getFileTimestamp(), currentFileSignature);
			default:
				throw new IllegalStateException("unsupported file state " + persistedFileMetadata.getFileState());
			}
		}
		
		return currentFileMetadata;
    	
    }
    
	// ****************************************************************
    
}
