/*
 * Copyright 2009-2018 Cyril Briquet.
 * 
 * Licensed under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *  
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package org.bitbucket.simplewebsitesynchronizer.state;

import org.bitbucket.simplewebsitesynchronizer.sitemap.SitemapTimestamp;

/**
 * @author Cyril Briquet
 */
public class FileMetadata {

    private       FileState state;
    private       SitemapTimestamp timestamp;
    private final FileSignature signature;

	// ****************************************************************

    /** Constructs a new FileMetadata.
     * @param state file state
     * @param timestamp sitemap timestamp
     * @param signature file digital signature
     */
    public FileMetadata(FileState state, SitemapTimestamp timestamp, FileSignature signature) {

		if (state == null) {
			throw new NullPointerException("illegal file state");
		}
		if (timestamp == null) {
			throw new NullPointerException("illegal sitemap timestamp");
		}
		if (signature == null) {
			throw new NullPointerException("illegal file signature");
		}
    	
        this.state = state;
        this.timestamp = timestamp;
        this.signature = signature;
        
    }

	// ****************************************************************

    @Override
	public synchronized String toString() {

    	final StringBuilder sb = new StringBuilder();
    	sb.append(this.state).append(" ").append(this.timestamp).append(" ").append(this.signature);
    	
    	return sb.toString();

    }

	// ****************************************************************

    public synchronized FileState getFileState() {
        
    	return this.state;
	
    }

    public synchronized void setFileState(FileState fileState) {
        
    	if (fileState == null) {
    		throw new NullPointerException("illegal file state");
    	}
    	
    	this.state = fileState;
	
    }

    public synchronized SitemapTimestamp getFileTimestamp() {
        
    	return this.timestamp;
	
    }

    public synchronized void setFileTimeStamp(SitemapTimestamp fileTimestamp) {
        
    	if (fileTimestamp == null) {
    		throw new NullPointerException("illegal file timestamp");
    	}
    	
    	this.timestamp = fileTimestamp;
	
    }
    
    public synchronized FileSignature getFileSignature() {

        return this.signature;

    }

	// ****************************************************************

}
