/*
 * Copyright 2009-2018 Cyril Briquet.
 * 
 * Licensed under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *  
 *    http://www.apache.org/licenses/LICENSE-2.0
 *    
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package org.bitbucket.simplewebsitesynchronizer.state;

import static org.bitbucket.simplewebsitesynchronizer.FileUtils.writeFile;

import java.io.IOException;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;

import org.bitbucket.simplewebsitesynchronizer.sitemap.SitemapTimestamp;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

/** Website state.
 * @author Cyril Briquet
 */
public class WebsiteState implements Iterable<FileMetadata> {

    private final String websiteBaseDirectory;
    private final String persistedStatePath;
    private final Map<String, FileMetadata> stateMap;
    private final SitemapTimestamp timestamp;
    
	// ****************************************************************

    @SuppressFBWarnings("EI_EXPOSE_REP2")
	public WebsiteState(String websiteBaseDirectory, String statePath, Map<String, FileMetadata> stateMap, SitemapTimestamp timestamp) {

    	if (websiteBaseDirectory == null) {
    		throw new NullPointerException("null website base directory");
    	}
    	if (statePath == null) {
    		throw new NullPointerException("null state path");
    	}
    	if (stateMap == null) {
    		throw new NullPointerException("null state");
    	}
    	if (timestamp == null) {
    		throw new NullPointerException("illegal timestamp");
    	}
    	
    	this.websiteBaseDirectory = websiteBaseDirectory;
		this.persistedStatePath = statePath;
		this.stateMap = stateMap;
		this.timestamp = timestamp;
		
	}

	// ****************************************************************

	@Override
	public synchronized String toString() {

		final StringBuilder sb = new StringBuilder();
		
		sb.append("# simple-website-synchronizer state of ").append(this.websiteBaseDirectory).append(", retrieved on ").append(new Date()).append("\n\n");
		
		final Iterator<FileMetadata> it = this.stateMap.values().iterator();
		while (it.hasNext()) {
			final FileMetadata fileMetadata = it.next();
			if (fileMetadata.getFileState() == FileState.deleted) continue;
		    sb.append(fileMetadata).append("\n\n");
		}

		return sb.toString();
		
	}

	// ****************************************************************

	@Override
	public synchronized Iterator<FileMetadata> iterator() {
		
		return this.stateMap.values().iterator();
		
	}

	// ****************************************************************

    public synchronized String getWebsiteBaseDirectory() {

    	return this.websiteBaseDirectory;
    	
    }
    
    public synchronized SitemapTimestamp getTimestamp() {
    	
    	return this.timestamp;
    	
    }
    
	// ****************************************************************

    public synchronized void persist() throws IOException {

    	final String path = this.websiteBaseDirectory + this.persistedStatePath;
	    System.out.println("\tPersisting website state to " + path + "...\n");
		
		writeFile(path, this.toString());

    }
    
	// ****************************************************************

}
