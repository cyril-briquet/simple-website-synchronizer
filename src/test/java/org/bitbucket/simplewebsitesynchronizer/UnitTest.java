// UnitTest.java

package org.bitbucket.simplewebsitesynchronizer;

import org.testng.annotations.Test;

/**
 * @author Cyril Briquet
 */
public class UnitTest {

	// ****************************************************************
	
	@Test
	public void testFileAndDirectoryOrder() {
		
		/* certify that directories are persisted AND enumerated (by WebsiteState)
		 * BEFORE their children files and subdirectories, so that they are created, on the remote FTP server,
		 * by the FTP uploader (WebsiteUploader.upload()) before the children files and subdirectories are uploaded
		 * (this property depends on the comparator implemented by Map<String, FileMetadata> stateMap and WebsiteState,
		 * which is the total order on file paths stored in the website state -- see WebsiteStateMapLoader.processLine())
		 */
		
		System.out.println("unit test output");
		
	}
	
	/*
	****************************************************************
	
	improve logging and error reporting
	
	****************************************************************
	
	write unit tests to certify that files state is correctly updated for:
	* updated files
	* missing files
	* missing files reappearing as-is
	* missing files reappearring updated
	
	****************************************************************
	
	certify behavior on non-Linux platforms
	
	****************************************************************
	 */
	
    // ****************************************************************

}
